# Read Data
d <- read.table("../Data_extraction/data.txt")
# Convert to log
L <- data.frame(year = d$V1, publ = log(d$V2))
# Write to file
write.table(L, "./L.txt", sep="\t")
