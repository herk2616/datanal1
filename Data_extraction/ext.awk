# This is a filter. it only matches lines that start with <year>
# ^ is regex for start of line
# <year> is literal
/^<year>/
# Main body of code
{
	# this creates an array with the name of the year
	#  $1 = <year>2020<\year>, because its the first
	#  block if we split the line with spaces
	# substr cuts until the 7th character of $1
	#  and keeps the 7th 8th 9th 10th character (length 4)
	# Then it increments the array value by one
	arr[substr($1,7,4)]+=1
}
# This is the End statment.
# This runs after the rest of the code is completed
END
{
	# Loops i from 1900-2020
	for(i=1900;i<=2020;i++)
		# if the array length of name i
		# exist, meaning its not zero
		if(length(arr[i]))
		# print the year and the array value
		print i" "arr[i]
}
