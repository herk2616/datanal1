set key left
set xlabel "Index"
set ylabel "#Publications"
set title "Comparison"

set xrange [0:20]
set yrange [-50000:400000]

set style line 1 lc rgb '#b0b0b0' lt 1 lw 2
set style line 2 lc rgb '#720178' lt 2 lw 2
set style line 3 lc rgb '#017278' lt 2 lw 2

set boxwidth 0.6 relative
set style fill solid
set grid ytics ls 1

set tics font "Source code pro,10"
set term pdf size 9.5, 4 font "Source code pro,20"

set output "Lp_plot.pdf"
plot '../Data_analisis/Lp.txt' title "Original" with points ls 3, '../Data_analisis/Lp2.txt' title "Predicted" with points ls 2
