unset key
set xlabel "Year"
set ylabel "#Logarithmic Publications"
set title ""

set xrange [1900:2020]
set yrange [1:14]

set style line 1 lc rgb '#b0b0b0' lt 1 lw 2
set style line 2 lc rgb '#21a2a8' lt 1 lw 1
set style line 3 lc rgb '#017278' lt 2 lw 2

set boxwidth 0.6 relative
set style fill solid
set grid ytics ls 1

set tics font "Source code pro,10"
set term pdf size 9.5, 4 font "Source code pro,20"

set output "Lf_plot.pdf"
plot '../Data_analisis/Lf.txt' with points ls 3
