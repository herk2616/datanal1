unset key
set style line 1 lc rgb '#b0b0b0' lt 1 lw 2

set xrange [1.2:2.8]
set yrange [-5:15]

set mytics 5
set grid ytics mytics ls 1

set style fill solid 0.7 border -1
set style boxplot outliers pointtype 7
set style data boxplot

set tics font "Source code pro,10"
set term pdf size 9.5, 4 font "Source code pro,20"

set output "box_plot.pdf"
plot '../Data_analisis/L.txt' using (2):2:(0.5) lc "#017278" "cyan"
