---
title: "Data analysis Excersize 1"
author: "Irakleios Kopitas"
date: "16 Apr 2020"
geometry: margin=2cm
output: pdf_document
header-includes:
- \setlength{\parindent}{1em}
- \setlength{\parskip}{0em}
---
![Unipi](http://www.unipi.gr/unipi/odigos_spoudon/unipilogo.png)

\pagebreak

# Data parser
I chose the awk programming language for the data extraction.

The awk usage is "` awk 'program' file `". So to run my program first
"`cd /path/to/dblp-2020-04-01.xml`" and run :
\begin{center}\rule{6.2in}{0.4pt}\end{center}
```awk
awk '/^<year>/{arr[substr($1,7,4)]+=1}END{for(i=1900;i<=2020;i++)if(length(arr[i]))print i" "arr[i]}'
dblp-2020-04-01.xml
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

And in plain text in case of colors messing up stuff:

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```
awk '/^<year>/{arr[substr($1,7,4)]+=1}END{for(i=1900;i<=2020;i++)if(length(arr[i]))print i" "arr[i]}'
dblp-2020-04-01.xml
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

# Explaining the code

This is a filter.
It only matches lines that start with `<year>`.
`^` is regex for start of line.
`<year>` is literal.

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```awk
/^<year>/
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

Main body of code.
This creates an array with the name of the year.
`$1 = <year>2020<\year>`, because its the first
block if we split the line with spaces.
`substr` cuts until the 7th character of `$1`,
and keeps the 7th 8th 9th 10th character (length 4).
Then it increments the array value by one.

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```awk
{
	arr[substr($1,7,4)]+=1
}
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

This is the End statment.
This runs after the  rest of the code is completed.
Loops i from 1900..2020.
If the array length of name i exist, meaning its not zero.
Prints the year and the array value.

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```awk
END
{
	for(i=1900;i<=2020;i++)
		if(length(arr[i]))
		print i" "arr[i]
}
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

\pagebreak

# Raw data diagram

![Diagram](./Data_representation/raw_data.pdf)

Gnu plot code

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```octave
unset key
set xlabel "Year"
set ylabel "#Publications"
set title "Records in DBPL"
set xrange [1900:2020]
set yrange [0:400000]
set style line 1 lc rgb '#b0b0b0' lt 1 lw 2
set style line 2 lc rgb '#21a2a8' lt 1 lw 1
set style line 3 lc rgb '#017278' lt 2 lw 2
set boxwidth 0.6 relative
set style fill solid
set grid ytics ls 1
set tics font "Source code pro,10"
set term pdf size 9.5, 4 font "Source code pro,20"
set output "raw_data.pdf"
plot '../Data_extraction/data.txt' with points ls 3
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

\pagebreak

# R and data analysis

Firstly we read the data from the file and place them in a
table. Then we take the logarithmic value of all the publications
and create a new table. Also we save the data into L.txt

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```R
# Read Data
d <- read.table("../Data_extraction/data.txt")
# Convert to log
L <- data.frame(year = d$V1, publ = log(d$V2))
# Write to file
write.table(L, "./L.txt", sep="\t")
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

![Logarithmic Diagram](./Data_representation/L_plot.pdf)

Gnu plot Code


\begin{center}\rule{6.2in}{0.4pt}\end{center}
```octave
unset key
set xlabel "Year"
set ylabel "#Publications"
set title "Records in DBPL"
set xrange [1900:2020]
set yrange [1:14]
set style line 1 lc rgb '#b0b0b0' lt 1 lw 2
set style line 3 lc rgb '#017278' lt 2 lw 2
set boxwidth 0.6 relative
set style fill solid
set grid ytics ls 1
set tics font "Source code pro,10"
set term pdf size 9.5, 4 font "Source code pro,20"
set output "L_plot.pdf"
plot '../Data_analisis/L.txt' with points ls 3
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

\pagebreak

# Caching outliers

With the values in the L.txt we create a box diagram.

![Box Diagram](./Data_representation/box_plot.pdf)

Gnu plot code

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```octave
unset key
set style line 1 lc rgb '#b0b0b0' lt 1 lw 2
set xrange [1.2:2.8]
set yrange [-5:15]
set mytics 5
set grid ytics mytics ls 1
set style fill solid 0.7 border -1
set style boxplot outliers pointtype 7
set style data boxplot
set tics font "Source code pro,10"
set term pdf size 9.5, 4 font "Source code pro,20"
set output "box_plot.pdf"
plot '../Data_analisis/L.txt' using (2):2:(0.5) lc "#017278" "cyan"
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

\pagebreak

# More R

Based on the box graph I will remove the extreme values, also I will remove
year 2020, which even though its value is close to the median, it is clearly
out of place in the previous graphs.

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```R
# Read L
L <- read.table("./L.txt")

# Cut the outlier
Lf <- L[19:85, ]
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

![Modified Diagram](./Data_representation/Lf_plot.pdf)

Now we can just call the lineal model from R to make a model and we are done.

\pagebreak

# But first testing

The test is simple. We take random values from our set *(75% of our values)*
and we make a lineal model. If we can predict the values that we did not select,
model works.

\begin{center}\rule{6.2in}{0.4pt}\end{center}
```R
# Set Seed
set.seed(100)

# Get random indexes
Index <- sample(1:nrow(Lf), 0.75*nrow(Lf))

# Get indexed data for training
training_data <- Lf[Index, ]

# Get the rest of the data for testing
test_data <- Lf[-Index, ]

# Build the model
linMod <- lm(publ ~ year, data=training_data)

# Actual vs Pred
comparison <- data.frame(cbind(original=test_data$publ, predicted=predict(linMod, test_data)))

# Return our values into publications again
Lp <- data.frame(exp(comparison$original), exp(comparison$predicted))

# Check correlation
cor(comparison)

```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

The resulted values are :


\begin{center}\rule{6.2in}{0.4pt}\end{center}
```C
Num                Original                 Predicted
1                       159                  328.1262
2                       249                  415.8517
3                      1030                  751.9385
4                      1082                 1072.8243
5                      1589                 1359.6469
6                      1939                 1723.1524
7                      2663                 2183.8421
8                      3064                 2458.4988
9                      4864                 3948.7995
10                    38871                29586.5405
11                    60013                53498.0607
12                    75863                67800.9190
13                   129819               108900.6980
14                   151846               122596.8846
15                   223488               221678.3537
16                   244541               249558.3227
17                   257093               280944.6904

Correlation:
           original predicted
original  1.0000000 0.9947243
predicted 0.9947243 1.0000000
```
\begin{center}\rule{6.2in}{0.4pt}\end{center}

\pagebreak

![Comparison Diagram](./Data_representation/Lp_plot.pdf)
